// Multithreading , passing the result back using the thread function’s argument
#include <pthread.h>
#include <stdio.h>
void *Tenfold(void *arg); // Thread -function prototype
int main(void) 
{
   pthread_t thread_id;
   double number = 123.456; // Local variable on the stack
   pthread_create(&thread_id , NULL, Tenfold , (void*) &number);
   /*函数原型int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start routine) (void *), void *arg);
   其中arg是个空指针，如果我们规定一个int或者double的变量number作为pthread_create()的arg指针变量，
   则必须把number变成一个指向number的指针，也就是(void*)&number.*/
   pthread_join(thread_id , NULL); // Not receiving anything here
   printf("Result: %lf\n", number);
   return 0;
}
void *Tenfold(void *arg) 
{
   double *result = (double*) arg; // Cast void* to double*
   *result *= 10; // Pass the result back whence it came
   pthread_exit(NULL); // Not passing anything here
}

//Tenfold函数内部自己创了一个local double指针变量result，让result=arg也就是指向pthread_create()函数的&number参数(&number=*arg)
//由于此处Tenfold里的pthread_exit()不返回result，则main里的pthread_join()也只能接收NULL