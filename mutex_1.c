#define _GNU_SOURCE // Needed by usleep() in gcc
#include <pthread.h> // threading , mutexes
#include <unistd.h> // write(), usleep()
#include <string.h> // strcpy(), strlen()
#include <stdbool.h> // true/false
#define USEmutex true // Toggle true/false in order (not) to use the mutex
// Function prototypes and global variables:
void *printer(); // Thread funct. - no arg此处可以省略*
void_print_message(char message[100]);
pthread_mutex_t myMutex = PTHREAD_MUTEX_INITIALIZER; // Initialise mutex

int main(void) 
{
    pthread_t threadID[3];
    // Start several printer threads:
    for(int i=0; i<3; i++) 
    {pthread_create(&threadID[i], NULL, printer , NULL);}
    // Join the threads:
    for(int i=0; i<3; i++) {pthread_join(threadID[i], NULL);}
    return 0;
}
// Thread function:
void *printer() //此处函数名可以神略*
{
    char message[100];
    // Lock the mutex:
    if(USEmutex) pthread_mutex_lock(&myMutex);
    // Perform critical action , using the function print_message():
    strcpy(message , "I hope you are receiving me loud and clear...\n");
    print_message(message);
    // Unlock/release the mutex:
    if(USEmutex) pthread_mutex_unlock(&myMutex);
    pthread_exit(NULL);
}
// Slowly print a message to screen , character by character:
void print_message(char message[100]) 
{
    for(unsigned int i=0; i<strlen(message); i++) 
    {
        write(1, &message[i], 1);
        usleep(10 * 1000); // Sleep for 10ms
    }
}
 
