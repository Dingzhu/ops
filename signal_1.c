#define _POSIX_C_SOURCE 199309L // sigaction(), struct sigaction , sigemptyset()
#include <stdio.h> // printf , getchar
#include <string.h> // memset
#include <signal.h> // sigaction , sigemptyset , struct sigaction , SIGINT

volatile sig_atomic_t signalCount = 0;
void fuck(int T);
int main(void) 
{
    struct sigaction act, oldact;
    // Define SHR:
    memset(&act, '\0', sizeof(act)); // Fill act with NULLs by default
    act.sa_handler = fuck; // Set the custom SHR
    act.sa_flags = 0; // No flags , used with act.sa_handler
    sigemptyset(&act.sa_mask); // No signal masking during SHR execution

    // Install SHR:
    sigaction(SIGTSTP, &act, &oldact); // This cannot be SIGKILL or SIGSTOP
    printf("Counting Ctrl-Zs until you press Enter...\n");
    while(getchar() != '\n') {;}
    printf("\nCtrl -Z has been pressed %d times. Press Ctrl-Z again to pause.\n",signalCount);

    // Restore original SHR:
    sigaction(SIGTSTP , &oldact , NULL);
    // Wait for Ctrl-Z to exit:
    while(1);
return 0;
}
// SHR using sa_handler:
void fuck(int T) 
{
    printf(" -- Signal caught: %d\n", T);
    signalCount++;
}
 