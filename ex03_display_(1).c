#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

void PrintCharacters(char prType, unsigned long numberOfTimes, char prCharint, int argc, char*argv[]);

int main(int argc, char *argv[]) {
    unsigned long int numOfTimes;
    char printMethod, printChar;
    printMethod = argv[1][0];
    numOfTimes = strtoul(argv[2], NULL, 10);  // String to unsigned long
    //printChar = argv[3][0];    
    /* unsigned long index = 0;
    // char echoCommand[] = "/bin/echo -n  ";
       int i;
       switch(printMethod) 
       {
           case 'p':
           for(index = 0; index < numOfTimes; index++)
           {for (i=3; i<argc; i++)
                {printChar = argv[i][0]; 
                 printf("%c", printChar);
                }
           }
           break;
       }*/
    PrintCharacters(printMethod, numOfTimes, printChar, argc, argv);  // Print character printChar numOfTimes times using method printMethod
    printf("\n");  // Newline at end
  return 0;
}


  void PrintCharacters(char printMethod, unsigned long numberOfTimes, char printChar,int argc, char*argv[]) {
  unsigned long index = 0;
  // char echoCommand[] = "/bin/echo -n  ";
  int i;
  
  switch(printMethod) {
    /* case 'e':
    echoCommand[13] = printChar;  // Put character to be printed in string
    for(index = 0; index < numberOfTimes; index++) {
      system(echoCommand);
    }
    break;*/
     case 'p':
    for(index = 0; index < numberOfTimes; index++) {for (i=3; i<argc; i++){printChar = argv[i][0]; 
	printf("%c", printChar);}
    }
    break;
    /*case 'w':
    for(index = 0; index < numberOfTimes; index++) {
      write(1, &printChar, 1);
    }
    break;*/
    default:
    printf("INTERNAL ERROR: Unknown print type:  %s.  This should not happen!\n", &printMethod);
  }
}
