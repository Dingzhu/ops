#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/io.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/platform_device.h>

#define BUFFER_MAX (10)

#define LED_ENABLE (0)
#define LED_DISABLE (1)

#define OK (0)
#define ERROR (-1)

#define GPBCON (0x56000010)
#define GPBDAT (0x56000014)
#define GPBUP (0x56000018)


#define LEDBASE 0x56000010
#define LEDLEN 0x0c
#define GPSIZE (0x4)

struct cdev *gDev;
struct file_operations *gFile;
dev_t devNum;
unsigned int subDevNum = 1;
static void __iomem *led_base;
int reg_major = 232;
static struct resources *wdt_mem;
int reg_minor = 0;
unsigned long led_time = 1;/*闪灯的时间间隔,最小为1秒(默认)*/
int testOpen(struct inode *p, struct file *f)
{
    volatile unsigned long *tmp;
    tmp = (volatile unsigned long* )ioremap(GPBUP, GPSIZE);
    *tmp=(*tmp &(~(0x1<<5)))|(0x1<<5); //disable the pull up function
    tmp -(volatile unsigned long*)ioremap(GPBCON, GPSIZE);
    *tmp=(*tmp &((0x1<<10)))|(0x1<<10); //set GPB5 to output
    printk(KERN_EMERG"testOpen\r\n");
    return OK;
}
ssize_t testWrite(struct file *f, const char __user *u, size_t s, loff_t *l)
{
    printk(KERN_EMERG "testWrite\r\n");
    return OK;
}
ssize_t testRead(struct file *f, char __user *u, size_t s, loff_t *l)
{
    printk(KERN_EMERG " testRead\r\n");
    return OK;
}
//        turu on or off the led 1
int testIoctl(struct inode *node, struct file *f, unsigned int cmd, unsigned long arg)
{
    volatile unsigned long *tmp;
    switch(cmd)
    {
    case LED_ENABLE:
        tmp=(volatile unsigned long*)ioremap(GPBDAT, GPSIZE);
        *tmp=*tmp &(~(0x1<<5))|(0x1<<5);//set output to 1
        break;
    case LED_DISABLE:
        tmp = (volatile unsigned long*)ioremap(GPBDAT, GPSIZE);
        *tmp =*tmp &(~(0x1<<5))|(0x0<<5); //set output to 0
        break;
    default:
        printk(KERN_EMERG"testloctl arg error\r\n");
    }
    return 0;
}
static int __init s3c2410led_probe(struct platform_device *pdev)
{
    struct resource *res;
    int size;
    res =platform_get_resource(pdev, IORESOURCE_MEM, 0);
    if (res != NULL){
	printk(KERN_EMERG" memory resource specifed\n");
    }
    else if (res == NULL){
	printk(KERN_EMERG"no memory resource specifed\n");
        //dev_err(dev, " no memory resource specifed\n");
        return -ENOENT;
    }
    size=(res->end - res -> start)+1;//8
    printk(KERN_INFO"start: %x, len: %x\n", res->start, size);
    /*
                        wdt_mem= request_mem_region(res->start, size, pdev->name);
                        if (wdt_mem== NULL){
                        dev_err(dev, " failed to get memory region\n");
                        ret =-ENOENT);
                        goto err_req;
                                }*/
    //led_base =ioremap(res->start, size);
    devNum= MKDEV(reg_major, reg_minor);
    printk(KERN_EMERG"devNum is 0x%x\r\n", devNum);
    if(OK ==register_chrdev_region( devNum, subDevNum, "testchar"))
    {
        printk(KERN_EMERG"register_chrdev_region ok\r\n");
    }
    else{
        printk(KERN_EMERG"register_chrdev_region error\r\n");
        return ERROR;
    }
    gDev=kzalloc(sizeof(struct cdev), 0);
    gFile=(struct file_operations*)kzalloc( sizeof(struct file_operations), GFP_KERNEL);
    gFile->open = testOpen;
    gFile->read = testRead;
    gFile->write = testWrite;
    //gFile->ioct= testloctl;
    gFile->owner = THIS_MODULE;
            cdev_init(gDev, gFile);
    cdev_add(gDev, devNum, 3);
    return 0;
}//? end s3c2410led_probe ?
static int __exit s3c2410led_remove(struct platform_device *pdev)
{
    cdev_del(gDev);
    unregister_chrdev_region(devNum, subDevNum);
}

static struct resource s3c_led_resource[]={
    [0]={
    .start = LEDBASE,
    .end = LEDBASE + LEDLEN -1,
    .flags = IORESOURCE_MEM,//中断资源还是内存资源
}
};

static struct platform_device s3c_device_led = {
    .name = "s3c2410-led",
    .id = -1,//表示只有一个
    .num_resources = ARRAY_SIZE(s3c_led_resource),//num_resource整型值，表示资源个数
    .resource =s3c_led_resource,
};

static struct platform_driver s3c2410led_driver = {
    .probe = s3c2410led_probe,
    .remove = s3c2410led_remove,
    .driver = {
        .name = "s3c2410-led",
        .owner = THIS_MODULE,
    },
};
int __init CharDrvInit(void)
{
    platform_device_register(&s3c_device_led);
    //把平台设备结构体添加到链表里
    return platform_driver_register(&s3c2410led_driver);
}
void __exit CharDrvExit(void)
{
    platform_device_unregister(&s3c_device_led);
    platform_driver_unregister(&s3c2410led_driver);
    return;
}
module_init(CharDrvInit);
module_exit(CharDrvExit);
MODULE_LICENSE("GPL");