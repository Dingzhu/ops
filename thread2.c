// Multithreading example using a global variable for the return information
#include <pthread.h>
#include <stdio.h>
void *Tenfold(void *arg); // Thread -function prototype
double result; //此处规定了一个全局变量，日后在thread function Tenfold里会用到
int main(void) 
{
   pthread_t thread_id;
   double number = 123.456;
   double *myResult;
   pthread_create(&thread_id , NULL, &Tenfold , (void*) &number);//Tenfold=*Tenfold=&Tenfold
    /*函数原型int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start routine) (void *), void *arg);
   1).其中void *(*start routine) (void *)表示函数返回类型和参数类型都是void*。start_routine是一个指向函数地址的指针，
   而函数名Tenfold本质上就是指向函数地址的指针，Tenfold解引用出来的内容依旧是函数Tenfold地址，包括Tenfold自己的地址仍然是Tenfold地址，
   也就是Tenfold=*Tenfold=&Tenfold
   2).其中arg是个空指针，如果我们规定一个int或者double的变量number作为pthread_create()的arg指针变量，
   则必须把number变成一个指向number的指针，也就是(void*)&number.*/
   // We expect a pointer value and need a double pointer to receive it:
   /*下列pthread_join()函数想要接收&result，根据函数原型要求，必须要一个双重指针变量来接收，我们必须创造一个双重指针变量&myResult.
   然后pthread_join()函数会自动把myResult和pthread_exit()函数返回值&result对等*/
   pthread_join(thread_id , (void**) &myResult);
   printf("Result: %lf\n", *myResult);
   // Discouraged alternative:
   //pthread_join(thread_id , NULL);
   //printf("Result: %lf\n", result);
   printf("Tenfold:%p\n",Tenfold);
   printf("*Tenfold:%p\n",*Tenfold);
   printf("&Tenfold:%p\n",&Tenfold);
   return 0;
}
void *Tenfold(void *arg) 
{
   result = *(double*) arg; // Cast argument (back) to double* and dereference
   result *= 10;
   pthread_exit((void*) &result); // Return the address of the global variable
}

运行结果:
Result: 1234.560000
Tenfold:0x55564fe7f8a4
*Tenfold:0x55564fe7f8a4
&Tenfold:0x55564fe7f8a4
