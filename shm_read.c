#include <sys/mman.h> // shm_oprn(),mmap(),shm_unlink() etc
#include <fcntl.h> // O * constants
 #include <sys/stat.h> // fstat , stat struct
#include <string.h> // strcpy()
 #include <unistd.h> // close(), write()
#include <stdio.h> // perror()
 #include <stdlib.h> // exit()
 int main() {
int fd;
 struct stat statBuf; // Struct that contains file status info
char *shmAddr , shmName[9];

strcpy(shmName , "/mySHM");

// Open the existing SHM object:
 fd = shm_open(shmName ,O_RDONLY , 0);
if(fd == -1) {
 perror("shm_read: shm_open");
exit(1);
 }

 // Query the size the SHM:
if(fstat(fd, &statBuf) == -1) {
 perror("shm_read: fstat");
exit(1);
 }
 // Map shmAddr to fd (RW, shared , no offset):
shmAddr = mmap(NULL, statBuf.st_size , PROT_READ , MAP_SHARED , fd, 0);
 if(shmAddr == MAP_FAILED) {
perror("shm_read: mmap");
 exit(1);
}

// fd is no longer needed:
 close(fd);
 // Copy message to shmAddr:
write(1, "Message from shm_read: ", 23);
 write(1, shmAddr , statBuf.st_size);
write(1, "\n", 1);

// Unmap SHM:
 if(munmap(shmAddr , statBuf.st_size) == -1) {
perror("shm_read: munmap");
 exit(1);
}

// Mark SHM for removal:
 if(shm_unlink(shmName) == -1) {
perror("shm_read: shm_unlink");
exit(1);
}

return 0;
 }