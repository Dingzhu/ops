#include <linux/i2c-dev.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>

#define I2C_SLAVE 0x0703
/*
** ./i2c_test w num
** ./i2c_test r
*/
int main(int argc,char **argv){
    int fd;
    int device_addr = 0x50; /* The I2C address:AT24C08 */
    char register_add = 0x30; /* Device register to access也就device上面哪个存储地址 */
    int res;
    char wbuf[10];
    char rbuf[10];

    /*打开适配器*/
    fd = open("/dev/i2c/0", O_RDWR);
    if(fd<0){
        perror("open failed!\n");
        exit(1);
    }
    /*用ioctl将适配器和设备握手*/
    if (ioctl(fd, I2C_SLAVE, device_addr) < 0) {
        perror("ioctl failed!\n");
        exit(1);
    }
    if(strcmp(argv[1],"w")==0){
        /*写数据*/
        wbuf[0] = register_add;
        wbuf[1] = atoi(argv[2]);
        //write函数里面数据和地址刚好是两个字节，故写2
        if (write(fd, wbuf, 2) != 2) {
            perror("write failed!\n");
            exit(1);
        }
    }else{
        if (write(fd, &register_add, 1) != 1) {
            perror("write failed!\n");
            exit(1);
        }
        /*读数据*/
        /*Using I2C Read*/
        if (read(fd, &rbuf[0], 1) != 1) {
            perror("read failed!\n");
            exit(1);
        } else {
            printf("rbuf[0]=%d\n",rbuf[0]);
        }
    }
    return 0;
}
