#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

int main(void) 
{
	printf("PID of initial process: %d\n", (int) getpid());
	printf("PPID of initial process: %d (the shell was I started from?)\n",(int) getppid());
        // Up to this point , only the initial process exists
	switch(fork()) //fork1
        { // Both parent and child 1 carry out switch()
 		case -1: // Initial process
		     printf("Fork failed.\n");
 		break;
 		case 0: // Child 1:
	             printf(" Child 1: My PID = %d\n", (int) getpid());
 		     printf(" Child 1: My parent’s PID = %d\n", (int) getppid());
 		     switch(fork()) //fork2
                     { // Both child 1 and grandchild carry out switch() 
                          case -1: // Child 1:
 		              printf("Fork for grandchild failed.\n");
           	          break;
                          case 0: // Grandchild:
                              printf(" Grandchild: My PID = %d\n", (int) getpid());
                              printf(" Grandchild: My parent’s PID = %d\n", (int) getppid());
                          break;
                          default: // Child 1:
                              printf(" Child 1 after fork: My PID = %d\n", (int) getpid());
                              printf(" Grandchild(wait1 should =grandchild) with PID %d has stopped\n", wait(NULL));//wait1 for fork2
                          break;
                     }
                break;
                default: // Parent:
                     printf("Parent after 1st fork: My PID = %d\n", (int) getpid());

                     switch(fork())//fork3
                         { // Both parent and child 2 carry out switch()
                          case -1: // Parent:
                          printf("Second fork failed.\n");
                          break;
                          case 0: // Child 2:
                              printf(" Child 2: My PID = %d\n", (int) getpid());
                              printf(" Child 2: My parent’s PID = %d\n", (int) getppid());
                          break;

                          default: // Parent:
                              printf("Parent after 2nd fork: My PID = %d\n", (int) getpid());
                              printf(" Child(wait2 should =child2) with PID %d has stopped\n", wait(NULL));//wait 2 for fork3
                              printf(" Child(wait3 should =child1) with PID %d has stopped\n", wait(NULL));//wait 3 for fork1
                          break;
                         }
       break;
       }
 // From here on, all of parent, child 1, child 2 and grandchild run code
printf("Process with PID %d now stops\n", (int) getpid());
return 0;
}

