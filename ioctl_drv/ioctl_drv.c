#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

//define the ioctl code
#define WR_DATA _IOW('a','a',int32_t*);//write data
#define RD_DATA _IOR('a','b',int32_t*);//read data

int main()
{
    int fd;
    int32_t val;
    printf("\n IOCTL based Character device driver operation from user space. \n");
    //open处“my_device”源于device_create(dev_class,NULL,dev,NULL,"my_device")，此函数在/dev下创建device node
    fd=open("/dev/my_device",O_RDWR);
    if(fd< 0 ){(printf("cannot open the device file .\n");
        return 0;
    }
    printf("Enter the data to send.\n");
    scanf("%d", &num);
    
    printf("Writing value to the driver. \n");
    ioctl(fd, WR_DATA, (int32_t*)&num);
    printf("Reading value from driver .\n");
    ioctl(fd, RD_DATA, (int32_t*)&val);
    printf("Closing driver. \n");
    close(fd);
}