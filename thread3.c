// Multithreading using a heap variable to return information
// Adapted from stackoverflow.com/a/8515532/1386750
// Note: malloc() is thread safe (see man) on Linux , but not reentrant
// (stackoverflow.com/a/856175/1386750)
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h> // malloc(), free()
void *Tenfold(void *arg); // Thread -function prototype
int main(void) 
{
   pthread_t thread_id;
   double input = 123.456;
   double *result;

   pthread_create(&thread_id , NULL, &Tenfold , &input);

   // We expect a pointer value and need a double pointer to receive it:
   pthread_join(thread_id , (void**) &result);
   printf("Result: %lf\n", *result);
   free(result); // Free the memory
   return 0;
}

void *Tenfold(void *arg) 
{
   double *retval = malloc(sizeof(double)); // Put the return value on the heap
   *retval = *(double*) arg; // Cast, dereference , copy the value
   *retval *= 10;
   pthread_exit((void*) retval); // Return the global address for the result
}