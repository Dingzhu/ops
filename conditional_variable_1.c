#define _GNU_SOURCE // Needed by usleep() in gcc
#include <pthread.h> // threading , mutexes , CVs
#include <stdio.h> // sprintf()
#include <unistd.h> // write(), usleep()
#include <stdbool.h> // true, false
#include <string.h> // strlen()

#define useMutex true // Toggle to (not) use the mutex (T/F)
#define useCV true // Toggle to (not) use the conditional variable (T/F)
#define pauseProd false // Pause after prod. to allow cons. to wake up (T/F)
// Function prototypes:
void *producer(void *arg), *consumer(void *arg); // Thread funcs - no argument
void print_message(char message[100], int sleepTime);

// Global variables:
volatile int MYSTOCK = 0;
pthread_mutex_t myMutex = PTHREAD_MUTEX_INITIALIZER; // Initialise a mutex
pthread_cond_t myCondVar = PTHREAD_COND_INITIALIZER; // Initialise a CV
int main(void) 
{
    pthread_t threadIDp ,threadIDc;
    unsigned int sleepTimeP=25, sleepTimeC=10; // Prod. slower than cons.
    // Start threads producer and consumer:
    pthread_create(&threadIDc , NULL, consumer , (void*) &sleepTimeC);
    pthread_create(&threadIDp , NULL, producer , (void*) &sleepTimeP);
    // Join threads:
    pthread_join(threadIDp , NULL);
    pthread_join(threadIDc , NULL);
    return 0;
}
// Thread function producer:
void *producer(void *arg) 
{
    int sleepTime = *(int*) arg;
    char message[100];
    for(int iter=0; iter <3; iter++) 
    { // Produce three Articles
        if(useMutex) pthread_mutex_lock(&myMutex); // Lock the mutex
        // Do critical action , using function print_message():
        sprintf(message , "Producer: slowly producing Article %i...\n", MYSTOCK+1);
        print_message(message , sleepTime);
        MYSTOCK += 1; // Update the global var.
        if(useCV) pthread_cond_signal(&myCondVar); // Signal condition change
        if(useMutex) pthread_mutex_unlock(&myMutex); // Unlock/release mutex
        if(pauseProd) usleep(1 * 1000); // 1ms; allow cons. wake up
    }
    pthread_exit(NULL);
}
// Thread function consumer:
void *consumer(void *arg) 
{
    int sleepTime = *(int*) arg;
    char message[100];
    // Do critical actions secured by the mutex:
    for(int iter=0; iter <3; iter++) 
    { // Consume three Articles
        if(useMutex) pthread_mutex_lock(&myMutex); // Acquire/lock the mutex
        // I can only consume if my stock is > 0:
        if(useCV && useMutex) 
        {
            while(MYSTOCK == 0) //此处while意思是如果stock里面0,则wait for signal.
            {pthread_cond_wait(&myCondVar , &myMutex); /*Block using CV myCondVar*/}
        }
        // Do critical action:
        sprintf(message , "Consumer: quickly consuming Article %i...\n", MYSTOCK);
        print message(message , sleepTime);
        MYSTOCK -= 1; // Update the global var.
        if(useMutex) pthread_mutex_unlock(&myMutex); // Unlock/release mutex
    }
pthread exit(NULL);
}
// Print a message to screen , character by character:
void print_message(char message[100], int sleepTime) 
{
    for(unsigned int i=0; i<strlen(message); i++) 
    {write(1, &message[i], 1);
     usleep(sleepTime * 1000); // Sleep for sleepTime ms
    }
}