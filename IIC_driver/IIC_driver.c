#include<linux/init.h>
#include<linux/module.h>
#include<linux/i2c.h>
#include<linux/fs.h>
#include<linux/device.h>
#include<linux/slab.h>
//#include<linux/uaccess.h>
#include<asm/uaccess.h>
#define E2PROM_MAJOR 100
struct e2prom_device{
    struct i2c_client   *at24c02_client;
    struct class        *at24c02_class;
    struct device       *at24c02_device;};
struct e2prom_device *e2prom_dev;
struct i2c_device_id e2prom_table[]={
    [0]={
    .name           ="24c02", 
    .driver_data    =0,
},
[1]={ 
    .name            ="24c08",
    .driver_data     =0,
}, 
};
static int i2c_read_byte (char *buf, int count){
    int ret=0;
    struct i2c_msg msg;
    msg.addr=e2prom_dev -> at24c02_client-> addr;//0X50
    msg.flags |=I2C_M_RD;                       //读
    msg.len=count;
    msg.buf=buf;
    ret=i2c_transfer(e2prom_dev->at24c02_client->adapter, &msg,1);
    if(ret<0){ 
        printk("i2c transfer failed!\n");
        return -EINVAL;
    }
    return ret;
}
static int i2c_write_byte(char *buf, int count)
{
    int ret=0;
    struct i2c_msg msg; 
    msg.addr        =e2prom_dev->at24c02_client->addr;//0x50
    msg.flags      =0;
    msg.len        =count;
    msg.buf        =buf;
    ret=i2c_transfer(e2prom_dev->at24c02_client->adapter, &msg, 1);
            if(ret<0){
            printk("i2c transfer failed!\n");
            return -EINVAL;
}
            return ret;
}
static int e2prom_open(struct inode *inode, struct file *file){
    return 0;}
static ssize_t e2prom_read (struct file*file, char __user *buf, size_t size, loff_t *offset)
{   int ret=0;
    char *tmp;
    tmp =kmalloc(size, GFP_KERNEL);
    if(tmp==NULL){
        printk("mallo failed! \n");     
        return -ENOMEM;
    }
    ret=i2c_read_byte(tmp, size);
    if(ret<0){
        printk("write byte failed! \n");
        ret =-EINVAL;
        goto  err0;
    }
    ret=copy_to_user(buf, tmp, size);
    if(ret){
        printk("copy data faile!\n");
        ret=-EFAULT;
        goto err0;
    }
    kfree(tmp);
    return size;
err0:
    kfree(tmp);
            return ret;
}
static ssize_t e2prom_write(struct file *file, const char __user *buf, size_t size, loff_t *offset)
{int ret=0;
    char *tmp;
    tmp =kmalloc(size, GFP_KERNEL);
    if(tmp==NULL){
        printk("mallo failed!\n");
        return -ENOMEM;}
    ret =copy_from_user(tmp, buf, size);
    if(ret){
        printk("copy data faile!\n");
        ret=-EFAULT;
        goto err0;}
    ret=i2c_write_byte(tmp, size);
    if(ret<0){
        printk("write byte failed!\n");
        ret=-EINVAL;
        
        goto err0;
    }
            kfree(tmp);
        return size;
err0:
        kfree(tmp);
        return ret;}
    
    struct file_operations e2prom_fops ={  
        .owner =THIS_MODULE,
                .open =e2prom_open,
                .write=e2prom_write,
                .read =e2prom_read,
    };
    static int e2prom_probe(struct i2c_client *client, const struct i2c_device_id *id)
    {int ret;
        printk("hello e2prom probe!!!!\n");
        //分配结构体空间
        e2prom_dev =kmalloc(sizeof(struct e2prom_device), GFP_KERNEL);
        if(e2prom_dev==NULL){
            printk(KERN_ERR "malloc failed!\n");
            return -ENOMEM;
        }
        //获取i2c设备结构体
        e2prom_dev->at24c02_client =client;
        /*给用户提供接口,注册i2c硬件操作方法*/
        ret=register_chrdev(E2PROM_MAJOR, "e2prom_module",&e2prom_fops);
        if (ret<0){
            printk(KERN_ERR "register major failed!\n");
            ret=-EINVAL;
            goto err0;
        }
        //创建设备类
        e2prom_dev->at24c02_class =class_create(THIS_MODULE, "e2prom_class");
        if(IS_ERR(e2prom_dev->at24c02_class)){
            printk(KERN_ERR "class create failed!\n");        
            ret =PTR_ERR(e2prom_dev->at24c02_class);
            goto err1;
        }
        //创建设备文件.创建/dev/at24c08
        e2prom_dev->at24c02_device=device_create(e2prom_dev->at24c02_class, NULL, MKDEV(E2PROM_MAJOR,0),NULL,"at24c08");
        if (IS_ERR(e2prom_dev->at24c02_device)){
            printk(KERN_ERR "class create failed!\n");
            ret =PTR_ERR(e2prom_dev->at24c02_device);
            goto err1;
        }
        return 0;
err1:
        unregister_chrdev(E2PROM_MAJOR, "e2prom_module");
err0:
        kfree(e2prom_dev);
        return ret; 
    }
    static int e2prom_remove(struct i2c_client *client){
        unregister_chrdev(E2PROM_MAJOR, "e2prom_module");
        device_destroy(e2prom_dev->at24c02_class, MKDEV(E2PROM_MAJOR, 0));
        class_destroy(e2prom_dev->at24c02_class);
        kfree(e2prom_dev);
        return 0;
    }
    /*1.构建一个 struct i2c_driver结构体*/    
    struct i2c_driver e2prom_driver = {
        .probe= e2prom_probe,
                .remove= e2prom_remove,
                .id_table=e2prom_table,//记录此驱动能服务于哪些设备
                .driver ={
                    .name="e2prom",
                },
    };
    static int __init e2prom_init (void)
    {   /*注册平台特定驱动
          1）将i2c驱动加入i2c总线的驱动链表
          2）搜索i2c总线的设备链表，每搜索一个都会调用i2c总线match
            实现client->name与id->name进行匹配，匹配成功就会调用
            i2c驱动中probe函数
        */ 
        
    i2c_add_driver(&e2prom_driver);
    return 0;
    }
    static void __exit e2prom_exit(void) 
    {i2c_del_driver(&e2prom_driver);//注销平台特定驱动
    }   
    module_init(e2prom_init);
    module_exit(e2prom_exit);
    MODULE_LICENSE("GPL");
