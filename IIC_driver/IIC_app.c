#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
/*at24c02_test w data,
/at24cO2_ test r
*/
int main(int argc, char **argv){
    int fd;
    char wbuf[2];
    char rbuf[2];
    char register_addr =0x05;
    fd =open("/dev/at24c08",O_RDWR);
    if(fd<0){
        perror("open /dev/at24c08");
        exit(1);
    }
   /*用ioctl将适配器和设备握手*/
   /*  if (ioctl(fd, 0x0703, register_addr) < 0) {
        perror("ioctl failed!\n");
        exit(1);
    }*/

    //写数据
    if(strcmp(argv[1],"w")==0){
        wbuf[0]=register_addr;
        wbuf[1]=atoi(argv[2]);
        if (write(fd, wbuf, 2) !=2){
            perror("write");
            exit(1);}}
    else{
        //读数据
        if(write(fd, &register_addr, 1)!=1){
            perror ("write");
            exit (1);}
        if(read(fd, rbuf, 1)!=1){
            perror("read");
            exit(1);
        }
        printf("rbuf[0]=%d\n", rbuf[0]); 
    }
}
