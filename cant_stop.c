#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include<stdlib.h>

void PrintCharacters(char prType, unsigned long numberOfTimes, char prCharint, int argc, char*argv[]);

int main(int argc, char *argv[]) {
    unsigned long int numOfTimes;
    char printMethod, printChar;
    printMethod = argv[1][0];
    numOfTimes = strtoul(argv[2], NULL, 10);  // String to unsigned long
    //printChar = argv[3][0];    
     unsigned long index = 0;
    // char echoCommand[] = "/bin/echo -n  ";
       int i;
       switch(printMethod) 
       {
           case 'p':
	   for(index = 0; index < numOfTimes; index++)
           {for (i=3; i<argc; i++)
                {
                  switch(fork())
		  {
                  case -1:
		    printf("failed");
		    break;
		  case 0:
		    nice((i-3)*19);
		    printChar = argv[i][0]; 
                    printf("%c", printChar);
		    
		  break;
		  default:
		    //printf("child with pid%d has finished",wait(NULL));
		    wait(NULL);
		    exit(0);
		  break;
		  }
                }
           }
           break;
       default:printf("err");
	 break;
       }
       printf("\n");  // Newline at end
       exit(0);
  return 0;
}
