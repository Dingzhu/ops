//install 32bit library for 64bit Ubuntu after 14...



sudo apt-get install lib32z1


sudo apt-get install lib32ncurses5 lib32z1

//NFS installation
//server:
$ sudo apt-get update
$ sudo apt install nfs-kernel-server

$ sudo mkdir -p /mnt/public

/*As we want all clients to access the directory, we will remove restrictive 
**permissions of the export folder through the following commands:*/
/*Now all users from all groups on the client system will be able to access our “sharedfolder”.*/
$ sudo chown nobody:nogroup /mnt/public
$ sudo chmod 777 /mnt/public
$ sudo chown nobody:nogroup /mnt/private
$ sudo chmod 777 /mnt/private

/*find broadcast ip of server*/
$ ip a


/*open /etc/exports with vi*/
$ sudo vi /etc/exports
/*add following at the end of /etc/exports*/
/mnt/public   *(rw,sync,no_subtree_check)
/mnt/private  192.168.0.104(rw,sync,no_subtree_check)


/**After making all the above configurations in the host system, now is the time to export the 
**shared directory through the following command as sudo:/
$ sudo exportfs -a
/*n order to make all the configurations take effect, restart the NFS Kernel server as follows:*/
$ sudo systemctl restart nfs-kernel-server


$ sudo ufw allow from 192.168.0.103/24 to any port nfs
$ sudo ufw allow from 192.168.0.100/24 to any port nfs
$ sudo ufw disable
$ sudo ufw status

$ sudo exportfs -arvf
$ sudo systemctl status nfs-kernel-server

/*查看服务器是否已经被挂载*/
$ showmount -e localhost
/*修改主机hosts内容，如果里面有client对应ip，去掉即可*/
$ sudo vi /etc/hosts

//NFS client:
$ sudo apt-get update
/*install the NFS Common client on your system:*/
$ sudo apt-get install nfs-common
/*Your client’s system needs a directory where all the content shared by the host server in the 
**export folder can be accessed. You can create this folder anywhere on your system. We are 
**creating a mount folder in the mnt directory of our client’s machine:*/
$ sudo mkdir -p /mnt/sharedfolder_client

/*Use the following command in order to mount the shared folder from the host to a mount folder 
**on the client:*/
/*here 192.168.0.103 is the host server ip
$ sudo mount 192.168.0.103:/mnt/public /mnt/sharedfolder_client

/*arm板子挂载命令：*/
$ mount -t nfs -o nolock 192.168.0.103:/mnt/public /mnt/public


/*查看客户端是否被挂载*/
$ showmount -e 192.168.0.103







